" Vim color scheme
" Name:			jsec.vim

set background=dark
highlight clear

if exists("syntax_on")
	syntax reset
endif

hi Normal 		guibg=Black 		guifg=#999999 		gui=NONE
"hi Normal 		guibg=Black 		guifg=#aaaaaa 		gui=NONE
hi Comment		guibg=Black			guifg=#606080		gui=NONE
"highlight Normal     guifg=Grey80	guibg=Black
hi Cursor		guibg=White			guifg=Black			gui=NONE
hi Special		guibg=Black			guifg=#ffff00
hi LineNr		guibg=Black			guifg=#a0a050		gui=NONE
hi Identifier	guibg=Black			guifg=#ffff00		gui=NONE

hi Function		guibg=Black			guifg=#00ff00		gui=NONE
hi Operator		guibg=Black			guifg=#ff00ff		gui=NONE

hi luaCond		guibg=Black			guifg=#ff4f00		gui=NONE
hi luaRepeat	guibg=Black			guifg=#ff4f00		gui=NONE
hi luaBlock		guibg=Black			guifg=#ff4f00		gui=NONE
hi Conditional  guibg=Black			guifg=#ff9f00		gui=NONE

hi luaConstant	guibg=Black			guifg=#ffffff		gui=NONE
hi luaTable 	guibg=Black			guifg=Green			gui=NONE
hi luaFunction	guibg=Black			guifg=Green			gui=NONE
hi luaStatement guibg=Black			guifg=#ff4f00		gui=NONE

hi luaFunc  	guibg=Black			guifg=Yellow		gui=NONE
"hi luaString  	guibg=Black			guifg=#004fff		gui=NONE
"hi  guibg=#ff00ff		guifg=#00ff00		gui=NONE

"types"
hi Number		guibg=Black			guifg=#ff0000		gui=NONE
hi String		guibg=Black			guifg=#00afff		gui=NONE
hi Boolean		guibg=Black			guifg=White			gui=NONE
hi luaString	guibg=Black			guifg=#009fff		gui=NONE

hi Pmenu		guibg=#202020		guifg=Yellow		gui=NONE
hi Pmenusel		guibg=Yellow		guifg=#202020		gui=NONE

"highlight Search     guifg=Black	guibg=Red	gui=bold
"highlight Visual     guifg=#404040			gui=bold
"highlight Cursor     guifg=Black	guibg=Green	gui=bold
"highlight Special    guifg=Orange
"highlight Comment    guifg=#80a0ff
hi StatusLine 	guibg=#ffff00		guifg=Black			gui=NONE
hi VertSplit 	guibg=Black 		guifg=White 		gui=none ctermfg=241
hi FoldColumn 	guibg=#202020 		guifg=grey40 		gui=none ctermfg=241


highlight Statement  guifg=Yellow			gui=NONE
highlight Type						gui=NONE





